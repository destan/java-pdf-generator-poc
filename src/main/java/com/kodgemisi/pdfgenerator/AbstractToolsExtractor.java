/*
 * Copyright © 2018 Kod Gemisi Ltd.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * This Source Code Form is “Incompatible With Secondary Licenses”, as defined by
 * the Mozilla Public License, v. 2.0.
 */

package com.kodgemisi.pdfgenerator;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.URL;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created on April, 2018
 *
 * @author destan
 */
@Slf4j
public abstract class AbstractToolsExtractor {

	//protected static final boolean isPosix = FileSystems.getDefault().supportedFileAttributeViews().contains("posix");

	private static final String RE_INIT_WARNING = "\nDid you call PdfGeneration#init() manually without setting lazy true or did you call it multiple times?";

	protected final Set<PosixFilePermission> perms;

	AbstractToolsExtractor() {
		HashSet<PosixFilePermission> permissions = new HashSet<>();
		permissions.add(PosixFilePermission.OWNER_EXECUTE);
		permissions.add(PosixFilePermission.OWNER_READ);

		this.perms = Collections.unmodifiableSet(permissions);
	}

	protected void extractPdfGenerationTools(Path destinationPath) throws IOException {
		final Instant start = Instant.now();

		if(destinationPath == null) {
			throw new IllegalArgumentException("destinationPath cannot be null");
		}

		final File destinationFile = destinationPath.toFile();
		destinationFile.deleteOnExit();

		if(!destinationFile.exists()) {
			throw new FileNotFoundException(destinationFile.getAbsolutePath());
		}
		if(!destinationFile.isDirectory()) {
			throw new IllegalArgumentException(destinationFile.getAbsolutePath() + " is not a directory!");
		}
		if(destinationFile.list().length > 0) {
			throw new IllegalStateException(destinationFile.getAbsolutePath() + " should be an empty directory!" + RE_INIT_WARNING);
		}
		if(!destinationFile.canWrite()) {
			throw new AccessDeniedException(destinationFile.getAbsolutePath());
		}

		log.debug("Using destinationPath {}", destinationPath);

		byte[] buffer = new byte[1024];
		final URL url = getZipUrl();

		log.trace("Using zip file {}", url);

		try(final InputStream is = url.openStream();final ZipInputStream zis = new ZipInputStream(is);) {

			ZipEntry zipEntry = zis.getNextEntry();
			while(zipEntry != null){
				final String fileName = zipEntry.getName();

				if(zipEntry.isDirectory()) {
					final Path newDir = Files.createDirectory(destinationPath.resolve(fileName));
					newDir.toFile().deleteOnExit();
				}
				else {
					final Path newFile = Files.createFile(destinationPath.resolve(fileName));
					newFile.toFile().deleteOnExit();

					try(FileOutputStream fos = new FileOutputStream(newFile.toFile());) {
						int len;
						while ((len = zis.read(buffer)) > 0) {
							fos.write(buffer, 0, len);
						}
					}

					if(newFile.toString().contains("/node/bin/") || newFile.toString().contains("/puppeteer/")) {
						final boolean executableResult = newFile.toFile().setExecutable(true);
					}

					//					final boolean readableResult = newFile.toFile().setReadable(true);
					//					if(log.isTraceEnabled()) {
					//						log.trace("{} : executable {}, readable {}", newFile.toString(), executableResult, readableResult);
					//					}

					//					if(isPosix && newFile.toString().contains("/bin/")) { // newFile.toString().contains("/bin/")
					//						Files.setPosixFilePermissions(newFile, perms);
					//					}
				}
				zipEntry = zis.getNextEntry();
			}
		}

		if(log.isTraceEnabled()) {
			log.trace("Extracted in " + (Duration.between(start, Instant.now()).get(ChronoUnit.NANOS)/1_000_000) + "ms");
		}
	}

	protected abstract URL getZipUrl();

	/**
	 *
	 * @return a list with initial capacity where there are 2 empty slots after platform dependent commands are appended to the list by implementing class.
	 */
	protected abstract List<String> getCommandArguments();

}
