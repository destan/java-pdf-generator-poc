/*
 * Copyright © 2018 Kod Gemisi Ltd.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * This Source Code Form is “Incompatible With Secondary Licenses”, as defined by
 * the Mozilla Public License, v. 2.0.
 */

package com.kodgemisi.pdfgenerator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created on April, 2018
 *
 * @author destan
 */
@Slf4j
public class PdfGeneration {

	private final Path thePath;

	private final ExecutorService executorService;

	private final ObjectMapper objectMapper = new ObjectMapper();

	private final AbstractToolsExtractor toolsExtractor;

	public PdfGeneration() {
		this(false);
	}

	public PdfGeneration(boolean lazy) {
		this(Executors.newCachedThreadPool(), lazy);
	}

	public PdfGeneration(ExecutorService executor, boolean lazy) {
		this.thePath = getExtractionDirectory();
		this.executorService = executor;

		try {
			final String osName = System.getProperty("os.name") == null ? "" : System.getProperty("os.name").toLowerCase();
			if (osName.startsWith("linux")) {
				toolsExtractor = (AbstractToolsExtractor) Class.forName("com.kodgemisi.pdfgenerator.LinuxToolsExtractor").newInstance();
			}
			else if (osName.startsWith("windows")) {
				toolsExtractor = (AbstractToolsExtractor) Class.forName("com.kodgemisi.pdfgenerator.WindowsToolsExtractor").newInstance();
			}
			else {
				throw new UnSupportedOperatingSystemException();
			}
		}
		catch (IllegalAccessException | InstantiationException e) {
			throw new IllegalStateException(e);
		}
		catch (ClassNotFoundException e) {
			log.error("Did you include operating system dependent library? " + e.getMessage());
			throw new IllegalStateException(e);
		}

		if (!lazy) {
			init();
		}
	}

	/**
	 *
	 */
	public void init() {
		try {
			toolsExtractor.extractPdfGenerationTools(thePath);
		}
		catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	public CompletableFuture<Boolean> generatePdf(URL url, PdfGenerationOptions options) {
		final CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();

		executorService.submit(() -> {

			final List<String> commandArguments = toolsExtractor.getCommandArguments();

			commandArguments.add(url.toString());

			try {
				commandArguments.add(objectMapper.writeValueAsString(options));
			}
			catch (JsonProcessingException e) {
				log.error(e.getMessage(), e);
				completableFuture.completeExceptionally(e);
			}

			// ProcessBuilder is not thread safe so every call needs a new instance
			final ProcessBuilder processBuilder = new ProcessBuilder();
			final Process process;
			try {
				if (log.isDebugEnabled()) {
					log.debug("Calling process with arguments {}", commandArguments);
				}
				process = processBuilder.directory(thePath.toFile()).command(commandArguments).start();

				StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), process.getErrorStream());
				executorService.submit(streamGobbler);
				int exitCode = process.waitFor();

				completableFuture.complete(exitCode == 0);
			}
			catch (Exception e) {
				log.error(e.getMessage(), e);
				completableFuture.completeExceptionally(e);
			}
		});

		return completableFuture;
	}

	private Path getExtractionDirectory() {

		final String destinationFromEnv = System.getenv("PDF_GENERATION_TOOL_PATH");

		if (destinationFromEnv != null && !destinationFromEnv.trim().isEmpty()) {
			log.info("PDF_GENERATION_TOOL_PATH is set to {}. Using it...", destinationFromEnv);
			return Paths.get(destinationFromEnv);
		}

		final String suffix = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss-SSS").withZone(ZoneId.systemDefault()).format(Instant.now());

		try {
			return Files.createTempDirectory("pdfGeneration_" + suffix);
		}
		catch (IOException e) {
			throw new IllegalStateException("Cannot create temp directory via Files.createTempDirectory!", e);
		}

	}

	//http://www.baeldung.com/run-shell-command-in-java
	private static class StreamGobbler implements Runnable {

		private InputStream inputStream;

		private InputStream errorStream;

		public StreamGobbler(InputStream inputStream, InputStream errorStream) {
			this.inputStream = inputStream;
			this.errorStream = errorStream;
		}

		@Override
		public void run() {
			try (InputStreamReader is = new InputStreamReader(inputStream);
					InputStreamReader es = new InputStreamReader(errorStream);
					BufferedReader ibf = new BufferedReader(is);
					BufferedReader ebf = new BufferedReader(es);) {

				if (log.isDebugEnabled()) {
					String input;
					while ((input = ibf.readLine()) != null) {
						log.debug(input);
					}
				}

				String error;
				while ((error = ebf.readLine()) != null) {
					log.error(error);
				}

			}
			catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}
}
