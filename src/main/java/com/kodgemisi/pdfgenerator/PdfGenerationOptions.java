/*
 * Copyright © 2018 Kod Gemisi Ltd.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * This Source Code Form is “Incompatible With Secondary Licenses”, as defined by
 * the Mozilla Public License, v. 2.0.
 */

package com.kodgemisi.pdfgenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.nio.file.Path;

/**
 * Created on April, 2018
 *
 * @author destan
 * @see <a href="https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#pagepdfoptions">Puppeteer API - PDF options</a>
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PdfGenerationOptions {

	private String path; // required

	private Double scale;

	private Boolean displayHeaderFooter;

	private String headerTemplate;

	private String footerTemplate;

	private Boolean printBackground;

	private Boolean landscape;

	private String pageRanges;

	private String format;

	private String width;

	private String height;

	private Margin margin;

	@AllArgsConstructor
	@NoArgsConstructor
	@Getter
	@Setter
	public static class Margin {
		private String top = "0";

		private String right = "0";

		private String bottom = "0";

		private String left = "0";
	}

	public static PdfGenerationOptions.PdfGenerationOptionsBuilder builder(Path path) {
		return new PdfGenerationOptions.PdfGenerationOptionsBuilder(path);
	}

	public static class PdfGenerationOptionsBuilder {
		private Path path;
		private Double scale;
		private Boolean displayHeaderFooter;
		private String headerTemplate;
		private String footerTemplate;
		private Boolean printBackground;
		private Boolean landscape;
		private String pageRanges;
		private String format;
		private String width;
		private String height;
		private PdfGenerationOptions.Margin margin;

		PdfGenerationOptionsBuilder(Path path) {
			this.path(path);
			this.margin = new Margin();
		}

		private PdfGenerationOptions.PdfGenerationOptionsBuilder path(Path path) {
			this.path = path;
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder scale(Double scale) {
			this.scale = scale;
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder displayHeaderFooter(Boolean displayHeaderFooter) {
			this.displayHeaderFooter = displayHeaderFooter;
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder headerTemplate(String headerTemplate) {
			this.headerTemplate = headerTemplate;
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder footerTemplate(String footerTemplate) {
			this.footerTemplate = footerTemplate;
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder printBackground(Boolean printBackground) {
			this.printBackground = printBackground;
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder landscape(Boolean landscape) {
			this.landscape = landscape;
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder pageRanges(String pageRanges) {
			this.pageRanges = pageRanges;
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder format(String format) {
			this.format = format;
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder width(String width) {
			this.width = width;
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder height(String height) {
			this.height = height;
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder marginTop(String marginTop) {
			this.margin.setTop(marginTop);
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder marginRight(String marginRight) {
			this.margin.setRight(marginRight);
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder marginBottom(String marginBottom) {
			this.margin.setBottom(marginBottom);
			return this;
		}

		public PdfGenerationOptions.PdfGenerationOptionsBuilder marginLeft(String marginLeft) {
			this.margin.setLeft(marginLeft);
			return this;
		}

		public PdfGenerationOptions build() {
			return new PdfGenerationOptions(this.path.toAbsolutePath().toString(), this.scale, this.displayHeaderFooter, this.headerTemplate, this.footerTemplate, this.printBackground, this.landscape, this.pageRanges, this.format, this.width, this.height, this.margin);
		}

		public String toString() {
			return "PdfGenerationOptions.PdfGenerationOptionsBuilder(path=" + this.path + ", scale=" + this.scale + ", displayHeaderFooter=" + this.displayHeaderFooter + ", headerTemplate=" + this.headerTemplate + ", footerTemplate=" + this.footerTemplate + ", printBackground=" + this.printBackground + ", landscape=" + this.landscape + ", pageRanges=" + this.pageRanges + ", format=" + this.format + ", width=" + this.width + ", height=" + this.height + ", margin=" + this.margin + ")";
		}
	}
}
