/*
 * Copyright © 2018 Kod Gemisi Ltd.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * This Source Code Form is “Incompatible With Secondary Licenses”, as defined by
 * the Mozilla Public License, v. 2.0.
 */

package com.kodgemisi.pdfgenerator;

/**
 * Created on April, 2018
 *
 * @author destan
 */
public class UnSupportedOperatingSystemException extends RuntimeException {

	UnSupportedOperatingSystemException() {
		super(System.getProperty("os.name"));
	}
}
