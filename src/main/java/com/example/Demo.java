package com.example;

import com.kodgemisi.pdfgenerator.PdfGeneration;
import com.kodgemisi.pdfgenerator.PdfGenerationOptions;
import lombok.extern.slf4j.Slf4j;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class Demo {

	// See pom.xml before running this class.

	public static void main(String[] args) throws MalformedURLException {
		PdfGeneration pdfGeneration = new PdfGeneration();

		final Path pdfFilePath = Paths.get(System.getProperty("user.home"), "Desktop", "demo.pdf");

		final PdfGenerationOptions options = PdfGenerationOptions
				.builder(pdfFilePath)
				.build();

		CompletableFuture<Boolean> future = pdfGeneration.generatePdf(new URL("https://kodgemisi.com"), options);

		future
		.thenAccept(aBoolean -> {
			log.info("Done with " + aBoolean);
			System.exit(0);
		})
		.exceptionally(e-> {
			log.error("ERROR: " + e.getMessage());
			System.exit(0);
			return null;
		});

		log.info("Main thread exits.");
	}
}
